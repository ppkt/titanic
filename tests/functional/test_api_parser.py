import pytest

from titanic.parsers.file import FileParser
from titanic.parsers.api import ApiParser


class TestApiParser:
    def test_fetch_url(self):
        api = ApiParser()
        _ = api.records

    @pytest.mark.parametrize(
        'file', ['json', 'csv'], indirect=['file'],
    )
    def test_api_content_the_same_as_file(self, file):
        expected_parser = FileParser(file)
        actual_parser = ApiParser()

        for expected, actual in zip(expected_parser.records, actual_parser.records):
            assert expected == actual
