import pytest

from titanic.exceptions import ParseError
from titanic.parsers.file import FileParser
from titanic.parsers.csv import CsvParser
from titanic.parsers.json import JsonParser


class TestParsers:
    @pytest.mark.parametrize(
        'parser, file', [(CsvParser, 'json'), (JsonParser, 'csv')], indirect=['file']
    )
    def test_import_invalid(self, parser, file):
        with pytest.raises(ParseError):
            parser(file)

    @pytest.mark.parametrize(
        'parser, file', [(CsvParser, 'csv'), (JsonParser, 'json')], indirect=['file']
    )
    def test_import_valid(self, parser, file):
        parser(file)

    def test_both_parsers_yields_same_results(self, csv, json):
        csv_parser = CsvParser(csv)
        json_parser = JsonParser(json)

        for csv_item, json_item in zip(csv_parser.records, json_parser.records):
            assert csv_item == json_item

    @pytest.mark.parametrize(
        'parser_class,file',
        [(JsonParser, 'json'), (CsvParser, 'csv')],
        indirect=['file'],
    )
    def test_correct_parser_is_used_csv(self, parser_class, file):
        parser_instance = FileParser.get_parser(file)
        assert isinstance(parser_instance, parser_class)

    @pytest.mark.parametrize(
        'file', ['json', 'csv'], indirect=['file'],
    )
    def test_file_parser_loads_file_correctly(self, file):
        parser = FileParser(file)
        assert parser.records
