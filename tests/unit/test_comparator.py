from dataclasses import astuple, dataclass, make_dataclass, replace

import pytest

from titanic.comparator import Comparator


@dataclass(frozen=True)
class DC:
    a: int
    b: str


@dataclass(frozen=True)
class BC:
    a: int
    b: str


BigDataClass = make_dataclass(
    'BigDataClass', ['a', 'b', 'c', 'd', 'e', 'f'], frozen=True
)


class TestComparator:
    def test_empty_lists(self):
        result = Comparator.compare([], [])
        assert result.equal

    def test_diff(self):
        a = [DC(1, 'a'), DC(2, 'b'), DC(3, 'c')]
        b = [DC(1, 'a'), DC(2, 'd'), DC(3, 'c')]
        result = Comparator.compare(a, b)

        assert result.changed_items == [(DC(2, 'b'), DC(2, 'd'))]
        assert not result.extra_items_a
        assert not result.extra_items_b

    def test_extra_items_b(self):
        a = [DC(1, 'a'), DC(2, 'b'), DC(3, 'c')]
        b = [DC(4, 'd')] + a
        result = Comparator.compare(a, b)

        assert not result.changed_items
        assert not result.extra_items_a
        assert result.extra_items_b == [DC(4, 'd')]

    def test_extra_items_a(self):
        a = [DC(1, 'a'), DC(2, 'b'), DC(3, 'c'), DC(4, 'd'), DC(5, 'e')]
        b = a[1:4]
        result = Comparator.compare(a, b)

        assert not result.changed_items
        assert result.extra_items_a == [DC(1, 'a'), DC(5, 'e')]
        assert not result.extra_items_b

    def test_one_list_has_extra_item_and_one_item_missing(self):
        base = [DC(1, 'a'), DC(2, 'b'), DC(3, 'c'), DC(4, 'd'), DC(5, 'e')]
        a = base[:3] + base[4:]
        b = base + [DC(6, 'f')]
        result = Comparator.compare(a, b)

        assert not result.changed_items
        assert not result.extra_items_a
        assert result.extra_items_b == [DC(4, 'd'), DC(6, 'f')]

    def test_ignore_columns(self):
        a = [
            BigDataClass(1, 1, 1, 1, 1, 1),
            BigDataClass(1, 1, 1, 1, 1, 2),
            BigDataClass(1, 1, 1, 1, 1, 3),
            BigDataClass(1, 1, 1, 1, 1, 4),
            BigDataClass(1, 1, 1, 1, 1, 5),
        ]
        b = [
            BigDataClass(5, 1, 1, 1, 1, 5),
            BigDataClass(6, 1, 1, 1, 1, 4),
            BigDataClass(7, 1, 1, 1, 1, 3),
            BigDataClass(8, 1, 1, 1, 1, 2),
            BigDataClass(9, 1, 1, 1, 1, 1),
        ]

        result = Comparator.compare(a, b, ignore_columns=['a'])
        assert result.equal
        assert not result.changed_items
        assert not result.extra_items_a
        assert not result.extra_items_b


class TestHelperFunctions:
    def test_get_threshold(self):
        assert Comparator.get_threshold(1, 1) == 0.0

        item = DC(1, 'a')
        assert Comparator.get_threshold(item, 0) == 1.0
        assert Comparator.get_threshold(item, 1) == 0.5
        assert Comparator.get_threshold(item, 2) == 0.0

    def test_similarity(self):
        assert Comparator.similarity_index(1, 2) == 0.0
        assert Comparator.similarity_index(DC(1, 'a'), BC(1, 'a')) == 0.0

    @pytest.mark.parametrize('differences', range(6))
    def test_similarity_differences(self, differences):
        item_1 = BigDataClass(1, 2, 3, 4, 5, 6)
        item_2 = list(astuple(item_1))

        # add as many differences as is provided in parameter
        for i in range(differences):
            item_2[i] = '9'

        item_2 = BigDataClass(*item_2)
        assert Comparator.similarity_index(item_1, item_2) == Comparator.get_threshold(
            item_1, differences=differences
        )
