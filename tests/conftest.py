from pathlib import Path

import pytest


@pytest.fixture
def current_dir(request) -> Path:
    """Get current directory"""
    return Path(request.fspath).parent


@pytest.fixture
def csv(current_dir: Path):
    """Return file handle to CSV fixture with passengers list"""
    # noinspection PyTypeChecker
    with open(current_dir / '../fixtures/valid/titanic-passengers.csv') as file:
        yield file


@pytest.fixture
def json(current_dir: Path):
    """Return file handle to JSON fixture with passengers list"""
    # noinspection PyTypeChecker
    with open(current_dir / '../fixtures/valid/titanic-passengers.json') as file:
        yield file


@pytest.fixture
def file(request):
    # workaround to handle issue from
    # https://github.com/pytest-dev/pytest/issues/349
    return request.getfixturevalue(request.param)
