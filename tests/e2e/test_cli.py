import os
import shlex
import subprocess
from pathlib import Path

import pytest


@pytest.fixture()
def root(request):
    """Return path to root dir"""
    return Path(request.fspath).parent.parent.parent


@pytest.fixture()
def html_report(tmp_path):
    """Return path to report file in temp dir"""
    yield tmp_path / 'report.html'


@pytest.fixture
def setup_test_env(current_dir, root):
    """Prepare environment to run CLI tool with coverage"""
    # create site config (required to "inject" code coverage)
    site = Path('sitecustomize.py')
    site.write_text("import coverage\ncoverage.process_startup()")

    # set PYTHONPATH (so site config could be found) and Coverage env variable
    # pointing to config file
    env = os.environ
    env['PYTHONPATH'] = env.get('PYTHONPATH', '') + f':{current_dir}'
    env['COVERAGE_PROCESS_START'] = os.fspath(root / '.coveragerc')
    yield env
    site.unlink()


@pytest.fixture
def run_titanic(setup_test_env, root):
    """Run titanic CLI within test environment"""
    env = setup_test_env

    def wrapper(args=''):
        """Wrapper for running script with coverage"""
        return subprocess.run(
            shlex.split(f'python -m titanic.cli {args}'),
            capture_output=True,
            env=env,
            cwd=root,
        )

    return wrapper


class TestCli:
    def test_titanic_no_args(self, run_titanic):
        result = run_titanic()
        assert b'usage: ' in result.stderr
        assert (
            b'error: the following arguments are required: input_file' in result.stderr
        )
        assert result.returncode == 2

    def test_titanic_help(self, run_titanic):
        result = run_titanic('-h')
        assert b'usage: ' in result.stdout
        assert result.returncode == 0

    def test_load_file_does_not_exist(self, run_titanic):
        result = run_titanic('definitely_does_not_exist.txt')
        assert result.returncode == 2
        assert not result.stdout
        assert "argument input_file: can't open" in result.stderr.decode()

    def test_load_invalid_file(self, run_titanic, tmp_path):
        p = tmp_path / "hello.txt"
        p.write_text('FILE WITH GARBAGE')
        result = run_titanic(os.fspath(p))
        assert result.returncode == 1
        assert not result.stdout
        assert "Not a CSV file" in result.stderr.decode()

    def test_valid_columns(self, run_titanic):
        result = run_titanic(
            '-c passengerid,age,ticket,sex tests/fixtures/valid/titanic-passengers.csv'
        )
        assert result.returncode == 0

    def test_invalid_columns(self, run_titanic):
        result = run_titanic(
            '-c age,notexist,fare tests/fixtures/valid/titanic-passengers.csv'
        )
        assert result.returncode == 2
        assert not result.stdout
        assert 'error: Columns not recognized: notexist' in result.stderr.decode()

    @pytest.mark.parametrize(
        'subset, expected_output',
        [
            (False, 'Both datasets are equal\n'),
            (True, 'All records from expected data were found in actual data\n'),
        ],
        ids=['subset enabled', 'subset disabled',],
    )
    @pytest.mark.parametrize('file', ['csv', 'json'])
    def test_load_file_and_compare_with_api_no_differences_expected(
        self, run_titanic, file, subset, expected_output
    ):
        result = run_titanic(
            f'{"-s" if subset else ""} tests/fixtures/valid/titanic-passengers.{file}'
        )
        assert result.returncode == 0
        assert not result.stderr
        assert result.stdout.decode() == expected_output

    @pytest.mark.parametrize('file', ['csv', 'json'])
    def test_load_file_with_extra_record(self, run_titanic, file):
        result = run_titanic(f'tests/fixtures/invalid/titanic-passengers-added.{file}')
        assert result.returncode == 1
        assert not result.stderr
        stdout = result.stdout.decode()
        assert 'Following *expected* items were not found in *actual* data' in stdout
        assert (
            "- Passenger(passengerid=1234, survived='Yes', pclass=3, name='Doe, Mr. "
            "John', sex='male', sibsp=0, parch=0, ticket='1234567', fare=7.75, age=None, "
            "embarked='Q', cabin=None)" in stdout
        )

    @pytest.mark.parametrize('file', ['csv', 'json'])
    def test_load_file_with_missing_record(self, run_titanic, file):
        result = run_titanic(
            f'tests/fixtures/invalid/titanic-passengers-removed.{file}'
        )
        assert result.returncode == 1
        assert not result.stderr
        stdout = result.stdout.decode()
        assert '*Actual* data contains items which were not *expected*' in stdout
        assert (
            "+ Passenger(passengerid=343, survived='No', pclass=2, name='Collander,"
            " Mr. Erik Gustaf', sex='male', sibsp=0, parch=0, ticket='248740', fare=13.0,"
            " age=28.0, embarked='S', cabin=None)" in stdout
        )

    @pytest.mark.parametrize('file', ['csv', 'json'])
    def test_load_file_with_changed_record(self, run_titanic, file):
        result = run_titanic(
            f'tests/fixtures/invalid/titanic-passengers-modified.{file}'
        )
        assert result.returncode == 1
        assert not result.stderr
        stdout = result.stdout.decode()
        assert 'Following items has different values' in stdout

        assert 'Attribute: pclass' in stdout
        assert 'Expected : 3' in stdout
        assert 'Actual   : 2' in stdout

        assert 'Attribute: age' in stdout
        assert 'Expected : 27.0' in stdout
        assert 'Actual   : 28.0' in stdout

    @pytest.mark.parametrize('file', ['csv', 'json'])
    def test_change_error_threshold_in_changed_file(self, run_titanic, file):
        result = run_titanic(
            f'--differences=1 '
            f'tests/fixtures/invalid/titanic-passengers-modified.{file}'
        )
        assert result.returncode == 1
        assert not result.stderr
        stdout = result.stdout.decode()
        assert 'Following *expected* items were not found in *actual* data' in stdout
        assert (
            " - Passenger(passengerid=343, survived='No', pclass=3, name='Collander, "
            "Mr. Erik Gustaf', sex='male', sibsp=0, parch=0, ticket='248740', fare=13.0, "
            "age=27.0, embarked='S', cabin=None)" in stdout
        )
        assert '*Actual* data contains items which were not *expected*' in stdout
        assert (
            " + Passenger(passengerid=343, survived='No', pclass=2, name='Collander, "
            "Mr. Erik Gustaf', sex='male', sibsp=0, parch=0, ticket='248740', fare=13.0, "
            "age=28.0, embarked='S', cabin=None)" in stdout
        )

    @pytest.mark.parametrize('file', ['csv', 'json'])
    def test_all_changes_in_one_file(self, run_titanic, file):
        result = run_titanic(
            f'tests/fixtures/invalid/titanic-passengers-all-changes.{file}'
        )
        assert result.returncode == 1
        assert not result.stderr
        stdout = result.stdout.decode()
        assert 'Following items has different values' in stdout

        assert 'Attribute: age' in stdout
        assert 'Expected : 26.0' in stdout
        assert 'Actual   : 25.0' in stdout

        assert 'Attribute: embarked' in stdout
        assert 'Expected : Q' in stdout
        assert 'Actual   : S' in stdout

        assert 'Following *expected* items were not found in *actual* data' in stdout
        assert (
            "- Passenger(passengerid=1234, survived='Yes', pclass=3, name='Doe, Mr. "
            "John', sex='male', sibsp=0, parch=0, ticket='1234567', fare=7.75, age=None, "
            "embarked='Q', cabin=None)" in stdout
        )
        assert '*Actual* data contains items which were not *expected*' in stdout
        assert (
            "+ Passenger(passengerid=343, survived='No', pclass=2, name='Collander,"
            " Mr. Erik Gustaf', sex='male', sibsp=0, parch=0, ticket='248740', fare=13.0,"
            " age=28.0, embarked='S', cabin=None)" in stdout
        )

    @pytest.mark.parametrize('file', ['csv', 'json'])
    def test_load_file_with_changed_record_ignore_one_column(self, run_titanic, file):
        result = run_titanic(
            f'-c pclass tests/fixtures/invalid/titanic-passengers-modified.{file}'
        )
        assert result.returncode == 1
        assert not result.stderr
        stdout = result.stdout.decode()
        assert 'Following items has different values' in stdout

        assert 'Attribute: pclass' in stdout
        assert 'Expected : 3' in stdout
        assert 'Actual   : 2' in stdout

        assert 'Attribute: age' not in stdout

    @pytest.mark.parametrize('file', ['csv', 'json'])
    def test_check_subset_mode(self, run_titanic, file):
        result = run_titanic(
            f'-s tests/fixtures/valid/titanic-passengers-subset.{file}'
        )
        assert result.returncode == 0
        assert not result.stderr
        assert (
            result.stdout.decode()
            == 'All records from expected data were found in actual data\n'
        )

    @pytest.mark.parametrize('file', ['csv', 'json'])
    def test_check_subset_mode_missing_items(self, run_titanic, file):
        result = run_titanic(
            f'-s tests/fixtures/invalid/titanic-passengers-subset-added.{file}'
        )
        assert result.returncode == 1
        assert not result.stderr
        stdout = result.stdout.decode()
        assert 'Following *expected* items were not found in *actual* data' in stdout
        assert (
            "- Passenger(passengerid=1234, survived='Yes', pclass=3, name='Doe, Mr. "
            "John', sex='male', sibsp=0, parch=0, ticket='1234567', fare=7.75, age=None, "
            "embarked='Q', cabin=None)" in stdout
        )

    @pytest.mark.parametrize('file', ['csv', 'json'])
    def test_check_subset_mode_valid_file(self, run_titanic, file):
        result = run_titanic(
            f'-s tests/fixtures/invalid/titanic-passengers-subset-modified.{file}'
        )
        assert result.returncode == 1
        assert not result.stderr
        stdout = result.stdout.decode()
        assert 'Following items has different values' in stdout
        assert 'Attribute: age' in stdout
        assert 'Expected : 19.0' in stdout
        assert 'Actual   : 9.0' in stdout

    @pytest.mark.parametrize('file', ['csv', 'json'])
    def test_generate_output(self, run_titanic, file, html_report):
        result = run_titanic(
            f'-r {os.fspath(html_report)} '
            f'tests/fixtures/invalid/titanic-passengers-all-changes.{file}'
        )
        assert result.returncode == 1
        assert html_report.exists()
        assert '<html>' in html_report.read_text()
        assert 'Equal items' in html_report.read_text()
        assert 'Equal items' in html_report.read_text()
        assert 'Modified elements' in html_report.read_text()
        assert 'Unexpected elements' in html_report.read_text()
        assert 'Expected elements not found' in html_report.read_text()
