# Titanic

Library for data comparision

[![pipeline status](https://gitlab.com/ppkt/titanic/badges/master/pipeline.svg)](https://gitlab.com/ppkt/titanic/-/commits/master)
[![coverage report](https://gitlab.com/ppkt/titanic/badges/master/coverage.svg)](https://gitlab.com/ppkt/titanic/-/commits/master)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

 * Python 3.8
 * tox

## Executing program
Small help is included:
```
$ titanic -h
usage: titanic [-h] [-c COLUMNS] [-d DIFFERENCES] [-s] [-r REPORT] input_file

positional arguments:
  input_file            expected data (in JSON or CSV format)

optional arguments:
  -h, --help            show this help message and exit
  -c COLUMNS, --columns COLUMNS
                        Check for differences only in provided columns (comma separated, no spaces, lowercase, order does not matter - see default). (default:
                        passengerid,survived,pclass,name,sex,sibsp,parch,ticket,fare,age,embarked,cabin)
  -d DIFFERENCES, --differences DIFFERENCES
                        How many differences could have record to be treated as modified. If there will be more differences, record will be marked as removed and added (default: 2)
  -s, --subset          If this flag is selected, checks whether expected data is a subset of actual dataset. (default: False)
  -r REPORT, --report REPORT
                        If this flag is enabled, a report will be generated showing differences and stored under provided filename. (default: None)
```

To compare differences between file and API:
```
$ titanic tests/fixtures/titanic-passengers.json
Both datasets are equal
```

In case of any differences, they'll be printed on screen:
```
Following *expected* items were not found in *actual* data
 - Passenger(passengerid=1234, survived='Yes', pclass=3, name='Doe, Mr. John', sex='male', sibsp=0, parch=0, ticket='1234567', fare=7.75, age=None, embarked='Q', cabin=None)

*Actual* data contains items which were not *expected*
 + Passenger(passengerid=127, survived='No', pclass=3, name='McMahon, Mr. Martin', sex='male', sibsp=0, parch=0, ticket='370372', fare=7.75, age=None, embarked='Q', cabin=None)

Following items has different values
Expected: Passenger(passengerid=130, survived='No', pclass=3, name='Ekstrom, Mr. John', sex='male', sibsp=0, parch=0, ticket='347061', fare=6.975, age=46.0, embarked='S', cabin=None)
Actual:   Passenger(passengerid=130, survived='No', pclass=3, name='Ekstrom, Mr. Johan', sex='male', sibsp=0, parch=0, ticket='347061', fare=6.975, age=45.0, embarked='S', cabin=None)
Differences:
  Attribute: name
    Expected : Ekstrom, Mr. John
    Actual   : Ekstrom, Mr. Johan

  Attribute: age
    Expected : 46.0
    Actual   : 45.0
```

### Subset mode
By adding parameter `-s` a behavior will change slightly - expected input file is treated as expected subset of actual output. Hence, extra elements in actual output won't be printed:
```
$ titanic -s tests/fixtures/valid/titanic-passengers-subset.csv
All records from expected data were found in actual data
```
Finding differences and removed records works as usual:
```
$ titanic -s tests/fixtures/invalid/titanic-passengers-subset-added.csv
Following *expected* items were not found in *actual* data
 - Passenger(passengerid=1234, survived='Yes', pclass=3, name='Doe, Mr. John', sex='male', sibsp=0, parch=0, ticket='1234567', fare=7.75, age=None, embarked='Q', cabin=None)
```
```
$ titanic -s tests/fixtures/invalid/titanic-passengers-subset-modified.csv
Following items has different values
Expected: Passenger(passengerid=183, survived='No', pclass=3, name='Asplund, Master. Clarence Gustaf Hugo', sex='male', sibsp=4, parch=2, ticket='347077', fare=31.3875, age=19.0, embarked='S', cabin=None)
Actual:   Passenger(passengerid=183, survived='No', pclass=3, name='Asplund, Master. Clarence Gustaf Hugo', sex='male', sibsp=4, parch=2, ticket='347077', fare=31.3875, age=9.0, embarked='S', cabin=None)
Differences:
  Attribute: age
    Expected : 19.0
    Actual   : 9.0
```
### Selecting columns
It's possible to compare using only selected columns by using `-c col1,col2,col3` parameter. For example:
```
$ titanic -c passengerid,survived,pclass,name,sex,sibsp,parch,ticket,fare,embarked,cabin tests/fixtures/invalid/titanic-passengers-modified.csv
```
Will omit `age` column in result:
```
Differences:
  Attribute: pclass
    Expected : 3
    Actual   : 2
```

While running command without parameter:
```
$ titanic tests/fixtures/invalid/titanic-passengers-modified.csv
```
Will show difference in `age` as well:
```
Differences:
  Attribute: pclass
    Expected : 3
    Actual   : 2

  Attribute: age
    Expected : 27.0
    Actual   : 28.0
```

### Generating reports
By adding `-r <PATH_TO_FILE>` parameter, a HTML file with report will be generated. In following example report will be stored in `output.html` file:
```
$ titanic -r output.html tests/fixtures/invalid/titanic-passengers-all-changes.csv
```

## Running tests
Tests are executed using pytest:
```
$ pip install tox
$ tox
```

### Mock API server
In order to speed up testing, it's possible to use mock server:
```
$ cd tests/mock_api
$ python3 mock_api.py
```
Server now listens on 127.0.0.1:8000 address and responds with contents of file `response.json` (file is loaded once, after modyfing it please restart the server).

To use mock server in tests, set a special env variable and run tests:
```
$ export TITANIC_DEVELOP=1
$ nox
```

## Code coverage
Coverage is enabled during test execution, to generate HTML report run:
```
$ coverage html
$ firefox html_cov/index.html
```

## Formatting the code
Black is used to enforce proper formatting:
```
$ tox -e format
```

## Building wheel package
Use setuptools:
```
$ python setup.py bdist_wheel
$ ls dist/
titanic-1.0.0-py3-none-any.whl
```
