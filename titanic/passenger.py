from dataclasses import dataclass
from typing import Optional


class Omit:
    """Class used to "omit" item value"""

    def __eq__(self, other):
        return True

    def __int__(self):
        return 0

    def __float__(self):
        return 0.0


@dataclass(frozen=True)
class Passenger:
    """Class representing passenger details"""

    passengerid: int
    survived: str
    pclass: int
    name: str
    sex: str
    sibsp: int
    parch: int
    ticket: str
    fare: float
    age: Optional[float] = None
    embarked: Optional[str] = None
    cabin: Optional[str] = None

    def __post_init__(self) -> None:
        # do post processing and handle typing
        # because class is 'frozen' - we can't assign values directly
        super().__setattr__('passengerid', int(self.passengerid))
        super().__setattr__('pclass', int(self.pclass))
        super().__setattr__('sibsp', int(self.sibsp))
        super().__setattr__('parch', int(self.parch))
        super().__setattr__('fare', float(self.fare))

        if self.age:
            age = float(self.age)
        else:
            # in CSV file, no value is an empty string, change it to None
            age = None
        super().__setattr__('age', age)

        # if there is no value for embarked and cabin, assume None
        if not self.embarked:
            super().__setattr__('embarked', None)

        if not self.cabin:
            super().__setattr__('cabin', None)
