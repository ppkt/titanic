class ParseError(Exception):
    """Raised when it's not possible to parse file."""
