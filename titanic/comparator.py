import dataclasses
from itertools import repeat
from operator import itemgetter
from typing import List, Optional

from titanic.passenger import Omit


@dataclasses.dataclass
class Result:
    """Class for storing comparison results"""

    list_a: List
    list_b: List
    changed_items: List = dataclasses.field(default_factory=list)
    extra_items_a: List = dataclasses.field(default_factory=list)
    extra_items_b: List = dataclasses.field(default_factory=list)
    equal_items: List = dataclasses.field(default_factory=list)

    @property
    def equal(self):
        """Return true if both lists are equal"""
        return not any((self.changed_items, self.extra_items_a, self.extra_items_b))


class Comparator:
    """Class responsible for comparing lists of dataclasses and finding differences"""

    @staticmethod
    def get_threshold(item: dataclasses.dataclass, differences: int) -> float:
        """Helper function to return threshold.

        Similarity threshold is a value to determine whether 2 records are similar but
        with modified values or two distinct records. `differences` parameter is used to
        calculate how many differences are allowed to treat item as "modified".

        :param item: dataclass instance used to calculate threshold value based on number
        of its fields
        :param differences: how many differences are allowed
        """
        if not dataclasses.is_dataclass(item):
            return 0.0
        attributes = len(dataclasses.astuple(item))
        return (attributes - differences) / attributes

    @staticmethod
    def similarity_index(item_a, item_b) -> float:
        """Naive implementation.

        Checks how many attributes has the same values in both items.
        Returns value 0.0 - 1.0 to indicate how "similar" are items to each other, where
        1.0 means identical. See `Comparator.get_threshold`."""

        if not dataclasses.is_dataclass(item_a) or not dataclasses.is_dataclass(item_b):
            # function works only on dataclasses instances
            return 0.0

        if type(item_a) is not type(item_b):
            # not possible to compare different dataclasses
            return 0.0

        tuple_a = dataclasses.astuple(item_a)
        tuple_b = dataclasses.astuple(item_b)

        same_values = sum(val_1 == val_2 for val_1, val_2 in zip(tuple_a, tuple_b))
        return same_values / len(tuple_a)

    @staticmethod
    def compare(
        list_a: List,
        list_b: List,
        threshold: Optional[float] = None,
        differences: int = 1,
        ignore_columns: Optional[List[str]] = None,
    ):
        """Compare two lists.

        Compare both lists and return the result of comparison.

        :param list_a: first list to compare
        :param list_b: second list to compare
        :param threshold: optional value in range 0.0 - 1.0 to used to determine how treat
         modified records, see `get_threshold`. If not provided, will be calculated using
         `differences` argument.
        :param differences: how many differences are allowed for two records to be treated
         as the same but modified. See `get_threshold`.
        :param ignore_columns: optional list with columns to ignore
        :return: result of comparison with lists of changed, removed, added and the same
         items
        """
        result = Result(list_a=list_a, list_b=list_b)

        if not (list_a or list_b):
            # both lists are empty
            return result

        if ignore_columns is not None:
            ignored_values = {k: v for k, v in zip(ignore_columns, repeat(Omit()))}

            # Iterate over all remaining objects and transform objects removing
            # provided columns
            new_list_a = []
            new_list_b = []
            for item in list_a:
                # replace ignored columns by special field
                new_list_a.append(dataclasses.replace(item, **ignored_values))

            for item in list_b:
                new_list_b.append(dataclasses.replace(item, **ignored_values))

            list_a = new_list_a
            list_b = new_list_b

        # 1. Remove common part from both lists
        for item in list_a:
            try:
                list_b.remove(item)
                result.equal_items.append(item)
            except ValueError:
                pass  # item not in list

        for item in result.equal_items:
            list_a.remove(item)

        # 1.2. If both lists are empty now - they're equal
        if not (list_a or list_b):
            return result

        # 2. Iterate over both lists to find "similar" items
        while list_a and list_b:
            item_a = list_a.pop()

            similar_items = []
            for item_b in list_b:
                similar_items.append(
                    (Comparator.similarity_index(item_a, item_b), item_b)
                )

            # 2.1. Sort lists based on similarity index
            similar_items = sorted(similar_items, key=itemgetter(0), reverse=True)

            # 2.2. Pick first (most similar) item and compare with similarity threshold
            similarity_index, most_similar_item = similar_items[0]

            if not threshold:
                # if threshold value was not provided, use "default" one
                threshold = Comparator.get_threshold(item_a, differences=differences)

            if similarity_index >= threshold:
                # treat items as the same but with modified value(s)
                result.changed_items.append((item_a, most_similar_item))
                list_b.remove(most_similar_item)
            else:
                # item is too distinct, add as "extra" item, not present in other list
                result.extra_items_a.append(item_a)

        # 3. All items from list A was compared with all items from list B
        #    Remaining items are "extra items"
        result.extra_items_a += list_a
        result.extra_items_b = list_b

        return result
