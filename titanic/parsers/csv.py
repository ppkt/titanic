import csv
import dataclasses

from ._abstract_file_parser import AbstractFileParser
from ..exceptions import ParseError
from ..passenger import Passenger


class CsvParser(AbstractFileParser):
    """Parser for CSV format."""

    def _load_data(self):
        super()._load_data()
        self.reader = csv.DictReader(
            self._fh, delimiter=';', quoting=csv.QUOTE_MINIMAL, strict=True
        )
        try:
            # lowercase key names
            self.header = [field.lower() for field in self.reader.fieldnames]
        except csv.Error:
            raise ParseError('Not a CSV file')

        if len(self.header) != len(dataclasses.fields(Passenger)):
            raise ParseError('Not a CSV file')

    def _process(self):
        for line in self.reader:
            yield Passenger(**{k: v for k, v in zip(self.header, line.values())})
