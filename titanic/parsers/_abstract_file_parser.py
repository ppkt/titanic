from abc import ABC

from ._abstract_parser import AbstractParser


class AbstractFileParser(AbstractParser, ABC):
    """Interface for parsers using files."""

    def _load_data(self):
        self._fh.seek(0)  # seek file to the beginning
