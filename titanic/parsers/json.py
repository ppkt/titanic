import json

from ._abstract_file_parser import AbstractFileParser
from ..exceptions import ParseError
from ..passenger import Passenger


class JsonParser(AbstractFileParser):
    """Parser for JSON format."""

    def _load_data(self):
        super()._load_data()
        try:
            self.json_data = json.load(self._fh)
        except json.JSONDecodeError:
            raise ParseError('Not a JSON file')

    def _process(self):
        file_content = [entry['fields'] for entry in self.json_data]
        for line in file_content:
            yield Passenger(**line)
