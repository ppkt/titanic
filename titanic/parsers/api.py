import json
import os
import urllib.parse
import urllib.request

from ._abstract_parser import AbstractParser
from ..exceptions import ParseError
from ..passenger import Passenger


class ApiParser(AbstractParser):
    """Parser for API format."""

    ROOT = 'https://public.opendatasoft.com/api/records/1.0/search/'
    PARAMS = {'dataset': 'titanic-passengers', 'rows': -1, 'sort': '-passengerid'}

    def _load_data(self):
        if os.environ.get('TITANIC_DEVELOP', False):
            # use mock server to speed things up
            self.ROOT = 'http://127.0.0.1:8000'

        with urllib.request.urlopen(
            f'{self.ROOT}?{urllib.parse.urlencode(self.PARAMS)}'
        ) as f:
            self.json_data = json.load(f)

    def _process(self):
        for record in self.json_data['records']:
            yield Passenger(**record['fields'])
