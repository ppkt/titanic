from __future__ import annotations

import io

from ._abstract_parser import AbstractParser
from .csv import CsvParser
from .json import JsonParser
from ..exceptions import ParseError


class FileParser:
    """Parser for provided file."""

    @staticmethod
    def get_parser(file: io.BufferedReader) -> AbstractParser:
        """Returns proper parser depending on file format.

        :param file: handle to opened file
        """
        try:
            return JsonParser(file)
        except ParseError:
            # it's not a valid json file, try CSV
            pass
        return CsvParser(file)

    def __init__(self, file: io.BufferedReader) -> None:
        """Load and parse file using proper parser

        :param file: file handle"""
        # pick parser depending on file content and load file
        parser = self.get_parser(file)

        # convert data from input format to internal structures
        self.records = parser.records
