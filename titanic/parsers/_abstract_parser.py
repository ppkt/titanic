import io
from abc import ABC, abstractmethod
from operator import attrgetter
from typing import Iterator, Optional, List

from ..passenger import Passenger


class AbstractParser(ABC):
    """Interface for different format parsers."""

    def __init__(self, file_handle: Optional[io.BufferedReader] = None) -> None:
        """Constructor.

        :param file_handle: handle to opened file, used to read content
        """
        self._fh = file_handle
        self._records = None
        self._load_data()

    @abstractmethod
    def _load_data(self) -> None:
        """Load and parse data from provided source.

        If it's not possible to process data (e.g. invalid format) raise an exception."""

    @abstractmethod
    def _process(self) -> Iterator[Passenger]:
        """Process parsed content.

        For every item (line, record) yield a `Passenger` instance."""

    @property
    def records(self) -> List[Passenger]:
        """Property to return Passenger records in unified format for each parser."""
        if not self._records:
            # sort content by 'passengerid' key
            self._records = sorted(list(self._process()), key=attrgetter('passengerid'))
        return self._records
