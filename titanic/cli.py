import argparse
import dataclasses
import io
import sys
from xml.etree import ElementTree as ET

from titanic.comparator import Comparator, Result
from titanic.parsers.api import ApiParser
from titanic.parsers.file import FileParser
from titanic.passenger import Passenger


def parse_args() -> argparse.Namespace:
    """Parse arguments"""
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        'input_file',
        help='expected data (in JSON or CSV format)',
        type=argparse.FileType('r'),
    )
    parser.add_argument(
        '-c',
        '--columns',
        help='Check for differences only in provided columns (comma separated, '
        'no spaces, lowercase, order does not matter - see default).',
        default=','.join(field.name for field in dataclasses.fields(Passenger)),
    )
    parser.add_argument(
        '-d',
        '--differences',
        help='How many differences could have record to be treated as modified. If '
        'there will be more differences, record will be marked as removed and '
        'added',
        type=int,
        default=2,
    )
    parser.add_argument(
        '-s',
        '--subset',
        help='If this flag is selected, checks whether expected data is a subset of '
        'actual dataset.',
        action='store_true',
    )
    parser.add_argument(
        '-r',
        '--report',
        help='If this flag is enabled, a report will be generated showing differences '
        'and stored under provided filename.',
        type=argparse.FileType('wb'),
    )
    parsed_args = parser.parse_args()

    # ensure that provided colums are valid
    columns_to_compare = set(
        column.strip() for column in parsed_args.columns.split(',')
    )
    all_columns = {field.name for field in dataclasses.fields(Passenger)}
    not_recognized_columns = columns_to_compare - all_columns

    if not_recognized_columns:
        parser.error(f'Columns not recognized: {", ".join(not_recognized_columns)}')
    parsed_args.ignore_columns = all_columns - columns_to_compare

    return parsed_args


def print_report(result: Result, subset_mode: bool) -> None:
    """Print report on stdout

    :param result: result of comparison operation
    :param subset_mode: if `True` use IN operator
    """
    if subset_mode:
        if not result.extra_items_a and not result.changed_items:
            print('All records from expected data were found in actual data')
            result.extra_items_b.clear()  # to proper set exit code
            return

    elif result.equal:
        print('Both datasets are equal')
        return

    if result.extra_items_a:
        print('Following *expected* items were not found in *actual* data')
        for item in result.extra_items_a:
            print(f' - {item}')
        print()

    if not subset_mode and result.extra_items_b:
        print('*Actual* data contains items which were not *expected*')
        for item in result.extra_items_b:
            print(f' + {item}')
        print()

    if result.changed_items:
        print('Following items has different values')
        for expected, actual in result.changed_items:
            expected_dict = dataclasses.asdict(expected)
            actual_dict = dataclasses.asdict(actual)
            print(f'Expected: {expected}')
            print(f'Actual:   {actual}')
            print('Differences:')
            for attribute in actual_dict.keys():
                if expected_dict[attribute] == actual_dict[attribute]:
                    continue

                print(f'  Attribute: {attribute}')
                print(f'    Expected : {expected_dict[attribute]}')
                print(f'    Actual   : {actual_dict[attribute]}')
                print()


def write_report(result: Result, report: io.TextIOBase, subset_mode: bool) -> None:
    """Print output to provided file

    :param result: instance with comparator results
    :param report: handle to file with report
    :param subset_mode: if `True` use IN operator for comparison
    """
    html = ET.Element('html')
    head = ET.SubElement(html, 'head')

    ET.SubElement(
        head,
        'link',
        rel='stylesheet',
        href='https://unpkg.com/purecss@1.0.1/build/pure-min.css',
        integrity='sha384-oAOxQR6DkCoMliIh8yFnu25d7Eq/PHS21PClpwjOTeU2jRSq11vu66rf90/cZr47',
        crossorigin='anonymous',
    )

    body = ET.SubElement(html, 'body')

    header = [field.name for field in dataclasses.fields(Passenger)]

    table = ET.SubElement(body, 'table')
    table.attrib['class'] = 'pure-table pure-table-bordered'

    # generate table header
    thead = ET.SubElement(table, 'thead')
    tr = ET.SubElement(thead, 'tr')
    for field in header:
        td = ET.SubElement(tr, 'td')
        td.text = field

    # generate table body
    tbody = ET.SubElement(table, 'tbody')

    def print_items(items, text, color=None):
        _tr = ET.SubElement(tbody, 'tr')
        _td = ET.SubElement(_tr, 'td', colspan=str(len(header)))
        _b = ET.SubElement(_td, 'b')
        _b.text = text
        for _item in items:
            _item_tuple = dataclasses.astuple(_item)
            _tr = ET.SubElement(tbody, 'tr')
            if color:
                _tr.attrib['style'] = f'background:{color}'
            for _value in _item_tuple:
                _td = ET.SubElement(_tr, 'td')
                _td.text = str(_value) if _value else ''

    # elements not found
    if result.extra_items_a:
        print_items(result.extra_items_a, 'Expected elements not found', 'red')

    # unexpected elements
    if result.extra_items_b and not subset_mode:
        print_items(result.extra_items_b, 'Unexpected elements', 'yellow')

    # modified elements
    if result.changed_items:
        tr = ET.SubElement(tbody, 'tr')
        td = ET.SubElement(tr, 'td', colspan=str(len(header)))
        b = ET.SubElement(td, 'b')
        b.text = 'Modified elements'
        for item_a, item_b in result.changed_items:
            item_a_tuple = dataclasses.astuple(item_a)
            item_b_tuple = dataclasses.astuple(item_b)
            tr = ET.SubElement(tbody, 'tr')
            for value_a, value_b in zip(item_a_tuple, item_b_tuple):
                td = ET.SubElement(tr, 'td')
                if value_a == value_b:
                    td.text = str(value_a) if value_a else ''
                else:
                    td.attrib['style'] = 'background:orange'
                    val_a = str(value_a) if value_a else ''
                    val_b = str(value_b) if value_b else ''
                    td.text = f'{val_a} -> {val_b}'

    if result.equal_items:
        # the same items
        print_items(result.equal_items, 'Equal items')

    ET.ElementTree(html).write(report, method='html')


def main() -> None:
    parsed_args = parse_args()

    expected_file = FileParser(parsed_args.input_file).records
    actual_file = ApiParser().records

    result = Comparator.compare(
        expected_file,
        actual_file,
        differences=parsed_args.differences,
        ignore_columns=parsed_args.ignore_columns,
    )
    print_report(result, parsed_args.subset)

    if parsed_args.report:
        write_report(result, parsed_args.report, parsed_args.subset)

    if result.equal:
        sys.exit(0)
    sys.exit(1)


if __name__ == '__main__':
    main()
