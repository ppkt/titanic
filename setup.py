from setuptools import find_packages, setup

setup(
    name='titanic',
    version='1.0.0',
    author='Karol Werner',
    packages=find_packages(),
    entry_points={'console_scripts': ['titanic = titanic.cli:main']},
    setup_requires=['wheel', 'tox'],
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: Console',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3.8',
        'Topic :: Software Development :: Testing',
    ],
)
